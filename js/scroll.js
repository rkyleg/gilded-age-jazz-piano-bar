/* global $ */
var counter = 0;
var lastScrollTop = 0;
var scrollDir = '';
$(document).ready(function() {
    $('a[href="' + document.referrer.split('//')[1].split('/')[1] + '"]').parent().removeClass('active');
    $('a[href="' + this.location.pathname.split('/')[1] + '"]').parent().addClass('active');

    // let Bootstrap handle the menu if on a phone or small tablet
    if(window.innerWidth >= 768){
       
        $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        if (st > window.lastScrollTop) {
            scrollDir = "down";
        }
        else {
            scrollDir = "up";
        }
        lastScrollTop = st;
        let navbarColor = "189,183,107"; //color attr for navbar (rgb)
        let smallLogoHeight = $('.small-logo').height();
        let smallLogoWidth = $('.small-logo').width();
        let bigLogoHeight = $('.big-logo').height();
        let navbarWidth = $('.navbar-collapse').width();
        let navbarHeight = $('.navbar-collapse').height();
        let mainNavWidth = $('.main-nav').width();
        let navbarRightWidth = $('.navbar-right').width();
        let smallLogoEndPos = (navbarHeight - smallLogoHeight) / 2;
        // var smallSpeed = (navbarHeight - smallLogoHeight);
        let ratio = (smallLogoHeight / bigLogoHeight);
        let ySmall = ($(window).scrollTop() * ratio);
        let smallPadding = navbarHeight - ySmall;
        if (smallPadding > navbarHeight) {
            smallPadding = navbarHeight;
        }
        if (smallPadding < smallLogoEndPos) {
            smallPadding = smallLogoEndPos;
        }
        if (smallPadding < 0) {
            smallPadding = 0;
        }
        if ($(window).scrollTop() < navbarHeight) {
            $('.small-logo').css({
                "margin-top": "10px"
            });
        }
        else {
            $('.small-logo').css({
                "margin-top": "0px",
                "display": "block"
            });
        }
        $('.small-logo-container ').css({
            "padding-top": smallPadding
        });
        if ($(window).scrollTop() > navbarHeight) {
            $('.main-nav').css({
                "padding-left": -10 + smallLogoWidth + (navbarWidth - smallLogoWidth - navbarRightWidth - mainNavWidth),
                // "float": "right"
            });
        }
        else {
            $('.main-nav').css({
                "padding-left": 0,
                // "float": "none"
            });
            // $('.navbar-right').addClass('pull-right');
        }
        var navOpacity = ySmall / smallLogoHeight;
        if (navOpacity > 1) {
            navOpacity = 1;
        }
        if (navOpacity < 0) {
            navOpacity = 0;
        }
        var navBackColor = 'rgba(' + navbarColor + ',' + navOpacity + ')';
        $('.navbar').css({
            "background-color": navBackColor
        });
        var shadowOpacity = navOpacity / 2;
        if (ySmall > 1) {
            $('.navbar').css({
                "box-shadow": "0 2px 3px rgba(0,0,0," + shadowOpacity + ")"
            });
        }
        else {
            $('.navbar').css({
                "box-shadow": "none"
            });
        }
        if (this.location.pathname.split('/')[1] == "about.html") {
            // event.preventDefault();
            // console.log(counter);
            // if(counter <= 1){
            //     $('#scroll-target-1').css({"display": "inline"});
            // }
            // if (counter <= 4 && scrollDir == "down") {
            //     counter++;
            //     console.log('down #scroll-target-' + counter);
            //     $('#scroll-target-' + counter).fadeIn();
            //     // $('#scroll-target-' + counter).animate({
            //     //     scrollTop: $("#scroll-container").offset().top + $("#scroll-container").outerHeight()
            //     // }, 1000);
            // }
            // if (counter > 0 && scrollDir == "up") {
            //     counter--;
            //     console.log('up #scroll-target-' + counter);
            //     // $('#scroll-target-' + counter).fadeOut();
            //     // $("#scroll-container").animate({
            //     //     scrollTop: $('#scroll-target-' + counter).offset().top
            //     // }, 1000);
            // }
            // else {
            //     // counter = 0;
            // }
        }
    });
    }
});
