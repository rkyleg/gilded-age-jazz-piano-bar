var mymap = L.map('mapid').setView([26.277224, -81.741206], 13);
var marker = L.marker([26.277224, -81.741206]).addTo(mymap);
L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'your.mapbox.project.id',
    accessToken: 'pk.eyJ1Ijoicmt5bGVnIiwiYSI6ImNpdnJpazFyNzAwMWsyb2x1YWVhMHphaTEifQ.BiGiuEP_8KB55dJi-jP1xg'
}).addTo(mymap);