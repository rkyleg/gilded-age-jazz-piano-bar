var bs = require("browser-sync").create();
console.log(process.env.NODE_PATH)
console.log(process.env.NVM_PATH)
// Start a Browsersync static file server
bs.init({
    server: {
        baseDir: ".",
        index: "index.html"
    },
    files: [
        "*.html",
        "css/*.css",
        "js/*.js"
    ],
    ui: false,
    notify: false,
    host: process.env.HOSTNAME,
    port: process.env.PORT
});
